FROM ubuntu:22.04
RUN apt update -y && \
	apt install -y --no-install-recommends \
		ca-certificates \
        binutils \
		curl \
        gpg \
        gpg-agent \
        dirmngr \
		unzip \
        ruby \
        python3 \
        python3-dev \
        python3-pip \
        python3-pyqt5 \
        python3-pyqt5.qtsql \
        python3-pyqt5.qtsvg \
        libqt5sql5-mysql \
        xvfb \
        mysql-client \
        python3-lxml \
        poppler-utils \
        build-essential \
        pyqt5-dev-tools

RUN python3 -m pip install --upgrade pip
RUN pip install pyinstaller poetry
ENV PYTHONPATH="/usr/lib/python3/dist-packages:${PYTHONPATH}"

ENV PATH="${PATH}:/root/.local/share/gem/ruby/3.0.0/bin"
RUN gem install fpm --user-install

WORKDIR /tmp
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini.asc /tini.asc
RUN gpg --batch --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 595E85A6B1B4779EA4DAAEC70B588DFF0527A9B7 && gpg --batch --verify /tini.asc /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]
